import numpy as np
import sympy as smp
from scipy.integrate import odeint
import matplotlib.pyplot as plt

t, m, g = smp.symbols('t m g')
theta = smp.Function('theta')(t) # Funkcja symboliczna reprezentująca kąt wychylenia wahadła w czasie
theta_d = smp.diff(theta, t) # 1 pochodna
theta_dd = smp.diff(theta_d, t) # 2 pochodna

path = 'parab' # Zmiana trakejtorii - ręczna
if path == 'taut':
    x = smp.sin(2 * theta) + 2 * theta
    y = 1 - smp.cos(2 * theta)
elif path == 'parab':
    x = theta
    y = theta ** 2

# Konwersja f. symbolicznych na f. numeryczne
x_f = smp.lambdify(theta, x)
y_f = smp.lambdify(theta, y)

# Lagrangian
T = 1 / 2 * m * ((smp.diff(x, theta) * theta_d) ** 2 + (smp.diff(y, theta) * theta_d) ** 2)
V = m * g * y
L = T - V

# Równanie Eulera-Lagrange'a
LE = smp.diff(L, theta) - smp.diff(smp.diff(L, theta_d), t)
LE = LE.simplify() # Równanie ruchu wahadła
deriv_2 = smp.solve(LE, theta_dd)[0] # Przyspieszenie kątowe (rozwiązanie względem 2 poch. thety)

# Konwersja f. symbolicznych na f. numeryczne
deriv2_f = smp.lambdify((g, theta, theta_d), deriv_2)
deriv1_f = smp.lambdify(theta_d, theta_d)

def dSdt(S, t, g): # Zwraca pochodne stanu S
    return [
        deriv1_f(S[1]),  # dtheta/dt
        deriv2_f(g, S[0], S[1])  # domega/dt
    ]


t_vals = np.linspace(0, 20, 1000)
g_val = 9.81
initial_conditions1 = [np.pi / 4, 0]
initial_conditions2 = [np.pi / 5, 0]

# Rozwiązanie numeryczne
ans1 = odeint(dSdt, initial_conditions1, t_vals, args=(g_val,))
ans2 = odeint(dSdt, initial_conditions2, t_vals, args=(g_val,))

# Rozwiązanie dokładne dla wahadła małych wychyleń
omega = np.sqrt(g_val)  # Dla małych wychyleń: omega = sqrt(g)
theta_exact1 = (initial_conditions1[0] * np.cos(omega * t_vals))
theta_exact2 = (initial_conditions2[0] * np.cos(omega * t_vals))

# Porównanie z rozwiązaniem dokładnym dla theta0 = pi/4
plt.figure()
plt.plot(t_vals, ans1[:, 0], label='Numeryczne theta0 = pi/4')
plt.plot(t_vals, theta_exact1, '--', label='Dokładne theta0 = pi/4')
plt.xlabel('Czas [s]')
plt.ylabel('Theta [rad]')
plt.legend()
plt.show()

# Porównanie z rozwiązaniem dokładnym dla theta0 = pi/5
plt.figure()
plt.plot(t_vals, ans2[:, 0], label='Numeryczne theta0 = pi/5')
plt.plot(t_vals, theta_exact2, '--', label='Dokładne theta0 = pi/5')
plt.xlabel('Czas [s]')
plt.ylabel('Theta [rad]')
plt.legend()
plt.show()

# Błędy
def calculate_errors(exact_solution, numerical_solution):
    abs_error = np.abs(exact_solution - numerical_solution)
    mse = np.mean((exact_solution - numerical_solution) ** 2)
    mae = np.mean(abs_error)
    return abs_error, mse, mae

abs_error1, mse1, mae1 = calculate_errors(theta_exact1, ans1[:, 0])
abs_error2, mse2, mae2 = calculate_errors(theta_exact2, ans2[:, 0])

# Wizualizacja błędów dla theta0 = pi/4
plt.figure()
plt.plot(t_vals, abs_error1, label='Absolute Error theta0 = pi/4')
plt.xlabel('Czas [s]')
plt.ylabel('Błąd')
plt.legend()
plt.show()

# Wizualizacja błędów dla theta0 = pi/5
plt.figure()
plt.plot(t_vals, abs_error2, label='Absolute Error theta0 = pi/5')
plt.xlabel('Czas [s]')
plt.ylabel('Błąd')
plt.legend()
plt.show()

print(f'Średni błąd bezwzględny (MAE) dla theta0 = pi/4: {mae1}')
print(f'Średni błąd kwadratowy (MSE) dla theta0 = pi/4: {mse1}')
print(f'Średni błąd bezwzględny (MAE) dla theta0 = pi/5: {mae2}')
print(f'Średni błąd kwadratowy (MSE) dla theta0 = pi/5: {mse2}')
